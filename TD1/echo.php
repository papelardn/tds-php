<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP : 
        <?php
          // Ceci est un commentaire PHP sur une ligne
          /* Ceci est le 2ème type de commentaire PHP
          sur plusieurs lignes */
           
          // On met la chaine de caractères "hello" dans la variable 'texte'
          // Les noms de variable commencent par $ en PHP
          $texte = "hello world !";

          // On écrit le contenu de la variable 'texte' dans la page Web
          echo $texte;



        $prenom = "Marc";
        $nom = "Husse";
        $login = "Marcus";

        echo ("L'utilisateur $prenom $nom possède le login $login");

        $array = array('prenom'=>'Marc', 'nom'=>'Husse','login'=>'Marcus');

        var_dump($array);

        echo ("L'utilisateur {$array['prenom']} {$array['nom']} possède le login {$array['login']}\n");

        $utilisateur[] = "Terhadeu";
        $utilisateur[] = "Gauwain";
        $utilisateur[] = "Exolga";
        var_dump($utilisateur);


        echo ("<H1>Liste des utilisateurs</H1>");
        echo ("<ul>");
        foreach ($utilisateur as $utilisateur) {
            echo ("<li>{$utilisateur}</li>");
        }
        echo ("</ul>");
        ?>




    </body>
</html> 