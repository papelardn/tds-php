<?php

require_once "ConnexionBaseDeDonnees.php";



class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() :string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
   ) {
        $this->login = substr($login,0,64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin(string $login)
    {
        $this->login = substr($login,0,64);
    }

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }



    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() :string {
        return "L'utilisateur $this->prenom, $this->nom sous le login $this->login";
    }


    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return  new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
}
    public static function recupererUtilisateurs() : array{
        $model = new ConnexionBaseDeDonnees();
        $pdoStatement = $model->getPdo()->query("SELECT * FROM utilisateur");
        $tableau = [];
        foreach($pdoStatement as $utilisateurFormatTableau){
            $utilisateur = new Utilisateur($utilisateurFormatTableau[0], $utilisateurFormatTableau[1], $utilisateurFormatTableau[2]);
            $tableau[] = $utilisateur;
        }
        return $tableau;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );


        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau == '') {
            return null;
        }

        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        echo($this->login);
        $sql = 'INSERT INTO utilisateur VALUES (:login, :nom, :prenom);';

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);


        $values = array(
            "login" => $this->login,
            "nom" => $this->nom,
            "prenom" => $this->prenom,
        );

        $pdoStatement->execute($values);


    }

}
?>

