<?php
require_once "ConfigurationBaseDeDonnees.php";
class ConnexionBaseDeDonnees
{
    private static ?ConnexionBaseDeDonnees $instance = null;
    private PDO $pdo;

    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=". ConfigurationBaseDeDonnees::getNomHote() .";port=". ConfigurationBaseDeDonnees::getPort() .";dbname=". ConfigurationBaseDeDonnees::getNomBaseDeDonnees() ,ConfigurationBaseDeDonnees::getLogin(),ConfigurationBaseDeDonnees::getMotDePasse(),
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    public static function getPdo(): PDO
    {
        return ConnexionBaseDeDonnees::getInstance()->pdo;
    }


    // getInstance s'assure que le constructeur ne sera
    // appelé qu'une seule fois.
    // L'unique instance crée est stockée dans l'attribut $instance
    private static function getInstance() : ConnexionBaseDeDonnees {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur
            ConnexionBaseDeDonnees::$instance = new ConnexionBaseDeDonnees();
        return ConnexionBaseDeDonnees::$instance;
    }

}